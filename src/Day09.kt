package net.keturn.advent2018

import kotlin.system.measureTimeMillis

const val SPECIAL_MARBLE_FACTOR = 23
const val NEXT_MARBLE_OFFSET = 1
const val TAKE_MARBLE_OFFSET = -7


fun inputMarbleGame(): Pair<Int, PointValue> {
    val infile = inputDirectory.resolve("9")
    val s = infile.readText()
    val match = Regex("""(\d+) players; last marble is worth (\d+) points""").find(s)!!
    val (playerCount, highestMarble) = match.groupValues.drop(1).map { it.toInt() }
    return Pair(playerCount, PointValue(highestMarble))
}


inline class PointValue(val value: Long): Comparable<PointValue> {
    constructor(value: Int) : this(value.toLong())

    override fun compareTo(other: PointValue): Int = this.value.compareTo(other.value)

    operator fun plus(other: PointValue): PointValue = PointValue(value + other.value)
    operator fun plus(marble: MarbleListEntry): PointValue = this + marble.value

    operator fun times(i: Long): PointValue = PointValue(value * i)

    inline val isSpecial: Boolean
        get() = (value != 0L) && (value % SPECIAL_MARBLE_FACTOR) == 0L
}

fun pointRange(low: PointValue, high: PointValue): Sequence<PointValue> {
    return sequence {
        for (i in low.value .. high.value) {
            yield(PointValue(i))
        }
    }
}

const val PROGRESS_INTERVAL = 250_000


class MarbleListEntry(val value: PointValue) {
    var previous: MarbleListEntry = this
    var next: MarbleListEntry = this
    var removed: Boolean = false

    fun remove(): MarbleListEntry {
        val nextCurrent = next
        previous.next = next
        next.previous = previous
        this.removed = true
        return nextCurrent
    }

    fun insertAfter(newValue: PointValue): MarbleListEntry {
        val newEntry = MarbleListEntry(newValue)
        newEntry.previous = this
        newEntry.next = next
        next.previous = newEntry
        next = newEntry
        return newEntry
    }

    fun forward(places: Int): MarbleListEntry {
        var currentMarble = this
        for (i in 1..places) {
            assert(currentMarble.next.previous === currentMarble) { "asymmetric links $currentMarble -> ${currentMarble.next} -< ${currentMarble.next.previous}" }
            currentMarble = currentMarble.next
        }
        return currentMarble
    }

    fun back(places: Int): MarbleListEntry {
        var currentMarble = this
        for (i in 1..places) {
            currentMarble = currentMarble.previous
        }
        return currentMarble
    }

    fun offset(places: Int): MarbleListEntry {
        return if (places >= 0) {
            forward(places)
        } else {
            back(-places)
        }
    }

    override fun toString(): String {
        return value.value.toString()
    }
}


data class MarbleGame(val playerCount: Int, val highestMarble: PointValue) {
    val playerScores = MutableList<PointValue>(playerCount) { PointValue(0) }
    var currentPlayer = 0
    val marbleZero = MarbleListEntry(PointValue(0))

    fun play(printEachStep:Boolean=false) {  // the game plays itself!
        var currentMarble = marbleZero

        var prevTimestamp = System.currentTimeMillis()
        var newTimestamp = prevTimestamp

        for (nextMarble in pointRange(PointValue(1), highestMarble)) {
            if ((nextMarble.value % PROGRESS_INTERVAL) == 0L) {
                newTimestamp = System.currentTimeMillis()
                println("%2.2f %d\t%d ms".format(
                    nextMarble.value.toFloat() / highestMarble.value * 100,
                    nextMarble.value, newTimestamp - prevTimestamp))
                prevTimestamp = newTimestamp
            }

            currentPlayer = (currentPlayer + 1) % playerCount
            if (printEachStep) {
                printMarbles(currentMarble)
            }
            if (nextMarble.isSpecial) {
                val oldMarble = currentMarble.offset(TAKE_MARBLE_OFFSET)
                playerScores[currentPlayer] += nextMarble + oldMarble
                currentMarble = oldMarble.remove()
            } else {
                currentMarble = currentMarble.offset(NEXT_MARBLE_OFFSET).insertAfter(nextMarble)
            }
        }
    }

    fun printMarbles(gameCurrentMarble: MarbleListEntry) {
        var printCurrentMarble = marbleZero

        do {
            if (printCurrentMarble === gameCurrentMarble) {
//                print("| %2s<(%2s)>%2s ".format(printCurrentMarble.previous, printCurrentMarble, printCurrentMarble.next))
                print("(%2d)".format(printCurrentMarble.value.value))
            } else {
//                print("| %2s< %2s >%2s ".format(printCurrentMarble.previous, printCurrentMarble, printCurrentMarble.next))
                print(" %2d ".format(printCurrentMarble.value.value))
            }
            printCurrentMarble = printCurrentMarble.next
        } while (printCurrentMarble != marbleZero)
        println("")
    }

    fun highScore(): PointValue {
        return playerScores.max()!!
    }
}

fun main(args: Array<String>) {
    val (playerCount, highestMarble) = inputMarbleGame()
    val game = MarbleGame(playerCount, highestMarble)
    println("${game.playerCount} players to marble ${game.highestMarble}")
    val gameTime = measureTimeMillis {
        game.play()
    }
    val highScore = game.highScore()
    println("$highScore points after $gameTime ms")


    println(" * * Part Two * *")
    val game2 = MarbleGame(playerCount, highestMarble * 100)
    println("${game2.playerCount} players to marble ${game2.highestMarble}")
    val game2Time = measureTimeMillis {
        game2.play()
    }
    val highScore2 = game2.highScore()
    println("$highScore2 points after $game2Time ms")
}