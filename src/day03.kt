package net.keturn.advent2018

import java.lang.RuntimeException
import kotlin.math.max
import kotlin.math.min
import kotlin.system.measureTimeMillis

const val CLOTH_WIDTH = 1000
const val CLOTH_HEIGHT = 1000

private fun inputClaims(): List<Claim> {
    val infile = inputDirectory.resolve("3")
    return infile.useLines {
        it.map { line -> Claim.fromString(line) }.toList()
    }
}

data class Claim(val id: Int, val left: Int, val top: Int, val width: Int, val height: Int) {
    companion object {
        // Example: #1 @ 662,777: 18x27
        private val pattern = Regex(
            """#(?<id>\d+)\s*@\s*(?<left>\d+),(?<top>\d+):\s*(?<width>\d+)x(?<height>\d+)"""
        )

        fun fromString(s: String): Claim {
            val matchStrings = pattern.find(s)!!
            val (id, left, top, width, height) = matchStrings.groupValues.drop(1).map(String::toInt)
            return Claim(id, left, top, width, height)
        }
    }

    val right: Int get() = left + width - 1
    val bottom: Int get() = top + height - 1

    fun collision(other: Claim): Rect? {
        if (this.bottom < other.top || other.bottom < this.top)
            return null
        if (this.right < other.left || other.right < this.left)
            return null

        val cLeft = max(this.left, other.left)
        val cTop = max(this.top, other.top)
        val cRight = min(this.right, other.right)
        val cBottom = min(this.bottom, other.bottom)

        return Rect(cLeft, cTop, cRight - cLeft + 1, cBottom - cTop + 1)
    }
}


data class Rect(val left: Int, val top: Int, val width: Int, val height: Int) {
    val right: Int get() = left + width - 1
    val bottom: Int get() = top + height - 1

    fun collided(other: Rect): Set<Rect>? {
        if (this.bottom < other.top || other.bottom < this.top)
            return null

        if (this.right < other.left || other.right < this.left)
            return null

        val inLeft: Rect
        val inRight: Rect
        if (this.left <= other.left) {
            inLeft = this
            inRight = other
        } else {
            inLeft = other
            inRight = this
        }

        // Special case for one rect being completely contained by the other.
        if (inRight.top >= inLeft.top &&
            inRight.bottom <= inLeft.bottom &&
            inRight.right <= inLeft.right  // and we already know inLeft.left <= inRight.left
        ) {
            return setOf(inLeft)
        }

        val leftRect = Rect(inLeft.left, inLeft.top, inRight.left - inLeft.left, inLeft.height)

        val collisionRect = Rect(
            left = inRight.left,
            top = min(inLeft.top, inRight.top),
            width = min(inLeft.right, inRight.right) - inRight.left + 1,
            height = max(inLeft.bottom, inRight.bottom) - min(inLeft.top, inRight.top) + 1
        )

        val rightRectFrom = if (inLeft.right > collisionRect.right) inLeft else inRight

        val rightRect = Rect(
            left = collisionRect.right + 1,
            top = rightRectFrom.top,
            width = rightRectFrom.right - collisionRect.right,
            height = rightRectFrom.height
        )

        return setOf(leftRect, collisionRect, rightRect).filter { it.width > 0 }.toSet()
    }

    val area: Int get() = width * height
}


fun partOne(claims: List<Claim>): Int {
    val collisions = findCollisions(claims)
    val union = unionRects(collisions)
    return sumArea(union)
}


fun findCollisions(claims: List<Claim>): List<Rect> {
    val leftToRight = claims.sortedBy { claim -> claim.left }
    val collisions = mutableListOf<Rect>()

    val notCollidedYet = claims.toMutableSet()

    for ((currentIndex, claim) in leftToRight.withIndex()) {
        for (hazard in leftToRight.listIterator(currentIndex + 1)) {
            if (hazard.left > claim.right) {
                break
            }
            val collision = claim.collision(hazard)
            if (collision != null) {
                collisions.add(collision)
                notCollidedYet.remove(hazard)
                notCollidedYet.remove(claim)
            }
        }
    }

    println("These ${notCollidedYet.size} never collided!")
    notCollidedYet.forEach { println(it) }

    return collisions.toList()
}


fun rasterizeClaims(claims: List<Claim>): Array<ShortArray> {
    val canvas = Array(CLOTH_WIDTH) { ShortArray(CLOTH_HEIGHT) }

    for (claim in claims) {
        for (column in claim.left until claim.left + claim.width) {
            for (row in claim.top until claim.top + claim.height) {
                val was = canvas[column][row]
                canvas[column][row] = (was + 1).toShort()
            }
        }
    }

    return canvas
}


fun countOverClaimed(canvas: Array<ShortArray>): Int {
    val totalClaimed = canvas.fold(0) { n, column -> n + column.count { it > 1 }}
    return totalClaimed
}


fun partOneRaster(claims: List<Claim>): Int {
    val canvas = rasterizeClaims(claims)
    return countOverClaimed(canvas)
}


fun unionRects(rects: List<Rect>): Set<Rect> {
    var oldGeneration = rects.toSet()

    val collisionFree = mutableSetOf<Rect>()

    var passCount = 0
    do {
        val leftToRight = oldGeneration.sortedBy { rect -> rect.left }.toMutableList()
        val notCollidedYet = oldGeneration.toMutableSet()
        val newGeneration = mutableSetOf<Rect>()

        println("* Pass ${passCount}, ${notCollidedYet.size} inputs, ${collisionFree.size} cleared")

        while (leftToRight.isNotEmpty()) {
            val rect = leftToRight.removeAt(0)
            val forRemoval = mutableListOf<Rect>()
            val forAddition = mutableSetOf<Rect>()

            bloop@ for (hazard in leftToRight) {
                if (hazard.left > rect.right) {
                    break@bloop
                }
                val results = rect.collided(hazard)

                if (results != null) {
                    if (results.all { it == rect}) {  // special case for eclipsing the other
                        forRemoval.add(hazard)
                        notCollidedYet.remove(hazard)
                    } else {
                        notCollidedYet.remove(rect)
                        notCollidedYet.remove(hazard)

                        forRemoval.add(hazard)
                        forAddition.addAll(results)
                        newGeneration.addAll(results)
                        notCollidedYet.addAll(results)
                    }
                }
            }
            leftToRight.removeAll(forRemoval)
            for (newRect in forAddition) {
                var insertionPoint = leftToRight.binarySearchBy(newRect.left) { it.left }
                if (insertionPoint < 0) {
                    insertionPoint = -insertionPoint - 1
                }
                leftToRight.add(insertionPoint, newRect)
            }
        }

        collisionFree.addAll(notCollidedYet)

        if (passCount++ > 999_999_999) {
            throw RuntimeException("Too many iterations!")
        }
        oldGeneration = newGeneration
    } while (newGeneration.isNotEmpty())

    println("* * DONE * * ${collisionFree.size} cleared")

    return collisionFree
}


fun sumArea(rects: Collection<Rect>): Int {
    return rects.fold(0) { acc, rect -> acc + rect.area }
}


fun main(args: Array<String>) {
    val claims = inputClaims()

    println("${claims.size} claims")

    var rasterOverlapArea: Int? = null
    val rasterPartOneTime = measureTimeMillis {
        rasterOverlapArea = partOneRaster(claims)
    }
    println("Overlapping area: ${rasterOverlapArea}, ${rasterPartOneTime} ms")

    var overlapArea: Int? = null
    val partOneTime = measureTimeMillis {
        overlapArea = partOne(claims)
    }
    println("Overlapping area: ${overlapArea}, ${partOneTime} ms")
}
