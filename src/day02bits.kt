package net.keturn.advent2018

/* INCOMPLETE experiments in more efficient comparison of codes. */

@ExperimentalUnsignedTypes
const val LOWEST_DIGIT: UByte = 97U  // 'a'.toByte().toUByte()
@ExperimentalUnsignedTypes
const val RADIX: UShort = 26U
@ExperimentalUnsignedTypes
const val BITS_PER_DIGIT = 5 // ceil(log(RADIX.toInt().toDouble(), 2.0))

@ExperimentalUnsignedTypes
fun inputSillyCodes(): Array<Pair<ULong, ULong>> {
    val infile = inputDirectory.resolve("2")
    val allBytes = infile.readBytes().asUByteArray()

    val output = allBytes.chunked(27) {chunk ->
        val (high, low) = chunk.chunked(13, ::bytesToHalfCode)

        Pair(high, low)
    }.toTypedArray()

    return output
}

@ExperimentalUnsignedTypes
fun bytesToHalfCode(bytes: List<UByte>): ULong {
    var result = 0UL
    for (b in bytes) {
        result *= RADIX
        result += (b - LOWEST_DIGIT)
    }
    return result
}

@ExperimentalUnsignedTypes
fun dayTwoPartTwoBitty(codes: Array<Pair<ULong, ULong>>): List<IntArray> {
    val pairIndicies = pairsOfTwo(codes.size)

    val nears = pairIndicies.filter { (i, j) -> isDistanceClose(codes[i], codes[j]) }

    return nears
}


@ExperimentalUnsignedTypes
fun isDistanceClose(a: Pair<ULong, ULong>, b: Pair<ULong, ULong>): Boolean {
    val highDiff = a.first.xor(b.first)
    val lowDiff = a.second.xor(b.second)

    if ((highDiff != 0UL) and (lowDiff != 0UL)) return false

    val highDistance = java.lang.Long.bitCount(highDiff.toLong())

    val highDiffStr = highDiff.toString(2)

    if (highDistance > BITS_PER_DIGIT) return false

    val lowDistance = java.lang.Long.bitCount(lowDiff.toLong())

    val lowDiffStr = lowDiff.toString(2)

    if (lowDistance > BITS_PER_DIGIT) return false

    return true
}


@ExperimentalUnsignedTypes
fun testCase() {
    val inputOne = ("lufjygedpvfbh" + "ftaxiwnaorzmq").toByteArray().toUByteArray()
    val inputTwo = ("lufjygedpvfbh" + "ftbxiwnaorzmq").toByteArray().toUByteArray()

    println("one")
    inputOne.chunked(13) {println(bytesToHalfCode(it))}
    println("two")
    inputTwo.chunked(13) {println(bytesToHalfCode(it))}
}

@ExperimentalUnsignedTypes
fun main(args: Array<String>) {
    val codes = inputSillyCodes()

    testCase()

    println(isDistanceClose(codes[77], codes[128]))
    // val nears = dayTwoPartTwoBitty(codes)
    //    println("${nears.size} close things: ${nears.toList().map {it.toList()}}")
}
