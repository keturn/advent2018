package net.keturn.advent2018

const val PLANT_SYMBOL = "🌲"  // evegreen tree
const val POT_SYMBOL = "⍽"  // open box

const val RULE_WIDTH = 5
const val INFLUENCE_DISTANCE = (RULE_WIDTH - 1) / 2
const val PART_ONE_GENERATIONS = 20
const val PART_TWO_GENERATIONS = 50_000_000_000


private fun poundToBool(it: Char): Boolean {
    return when (it) {
        '#' -> true
        '.' -> false
        else -> error("unexpected state indicator \"$it\"")
    }
}


fun poundToBoolArray(s: String): BooleanArray {
    return s.map(::poundToBool).toBooleanArray()
}


fun inputPlants(): Pair<BooleanArray, List<Pair<BooleanArray, Boolean>>> {
    return inputDirectory.resolve("12").useLines { lines ->
        lateinit var initialState: BooleanArray
        val rules = mutableListOf<Pair<BooleanArray, Boolean>>()
        lines.forEach { line ->
            when {
                line.startsWith("""initial state:""") -> {
                    initialState = poundToBoolArray(line.split(":", limit = 2)[1].trim())
                }
                "=>" in line -> {
                    val (inputStr, resultStr) = line.split("=>", limit = 2).map { it.trim() }
                    rules.add(inputStr.map(::poundToBool).toBooleanArray() to poundToBool(resultStr[0]))
                }
                line.isBlank() -> Unit
                else -> error("Unexpected input: $line")
            }
        }
        return@useLines Pair(initialState, rules)
    }
}


class PlantGeneration(private val plants: BooleanArray, val leftIndex: Int = 0) {
    constructor(plantString: String, leftIndex: Int = 0) : this(poundToBoolArray(plantString), leftIndex)

    fun checksum(): Long {
        return (leftIndex .. rightIndex).asSequence().map {
            if (isPlantAt(it)) it.toLong() else 0L
        }.sum()
    }

    fun isPlantAt(index: Int): Boolean {
        return this.plants.getOrElse(index - leftIndex) { false }
    }

    val rightIndex: Int get() = plants.lastIndex + leftIndex

    fun trim(): PlantGeneration {
        val first = plants.indexOfFirst { it }
        val last = plants.indexOfLast { it }
        return PlantGeneration(plants.sliceArray(first..last), leftIndex + first)
    }

    fun stableOffset(other: PlantGeneration): Int? {
        if (!plants.contentEquals(other.plants)) {
            return null
        }
        return leftIndex - other.leftIndex
    }

    fun countPlants(): Int {
        return plants.count { it }
    }

    override fun toString(): String {
        return plants.map {
            if (it) PLANT_SYMBOL else POT_SYMBOL
        }.joinToString("")
    }
}


data class StablePlants(val plants: PlantGeneration, val generation: Long, val offset: Int) {
    fun projectFutureChecksum(targetTime: Long): Long {
        val current = plants.checksum()
        val deltaTime = targetTime - generation
        return current + deltaTime * plants.countPlants() * offset
    }
}


class PlantBreeder(private val rules: List<Pair<BooleanArray, Boolean>>) {
    fun breed(plants: PlantGeneration): PlantGeneration {
        val newLeftIndex = plants.leftIndex - INFLUENCE_DISTANCE
        val newRightIndex = plants.rightIndex + INFLUENCE_DISTANCE

        return PlantGeneration(
            (newLeftIndex..newRightIndex).map { i ->
                evaluateAtIndex(plants, i)
            }.toBooleanArray(),
            newLeftIndex
        ).trim()
    }

    fun breedForGenerations(plants: PlantGeneration, generations: Int, print: Boolean = false): PlantGeneration {
        return breedForGenerations(plants, generations.toLong(), print)
    }

    fun breedForGenerations(plants: PlantGeneration, generations: Long, print: Boolean = false): PlantGeneration {
        var currentPlants = plants
        for (generation in (1..generations)) {
            currentPlants = breed(currentPlants)

            if (print) {
                val expectIndex = currentPlants.leftIndex - (generation * -INFLUENCE_DISTANCE)
                val padding = (generations - generation) * INFLUENCE_DISTANCE
                println("%3d %6d %3d %s%s%s".format(
                    generation,
                    currentPlants.checksum(),
                    currentPlants.rightIndex - currentPlants.leftIndex,
                    " ".repeat(padding.toInt()),  // geez no do not print these when over MAXINT
                    POT_SYMBOL.repeat(expectIndex.toInt()),
                    currentPlants
                ))
            } else if ((generation % 100_000) == 0L){
                println(generation)
            }
        }
        return currentPlants
    }

    fun breedUntilStable(plants: PlantGeneration): StablePlants {
        var currentPlants = plants
        var generation = 0L
        var stableOffset: Int?
        do {
            val nextPlants = breed(currentPlants)
            stableOffset = nextPlants.stableOffset(currentPlants)
            currentPlants = nextPlants
            generation++
        } while (stableOffset == null)

        println("Population is STABLE with shift %+d at gen %d".format(stableOffset, generation))
        println("%+d %s".format(currentPlants.leftIndex, currentPlants))

        return StablePlants(currentPlants, generation, stableOffset)
    }

    private fun evaluateAtIndex(plants: PlantGeneration, index: Int): Boolean {
        nextRule@ for ((rule, result) in rules) {
            for (i in (0 until RULE_WIDTH)) {
                if (plants.isPlantAt(index + i - INFLUENCE_DISTANCE) != rule[i]) {
                    continue@nextRule
                }
            }
            return result
        }
        return false
    }
}


fun day12partOne(initialState: BooleanArray, rules: List<Pair<BooleanArray, Boolean>>): Long {
    val breeder = PlantBreeder(rules)
    val gen0 = PlantGeneration(initialState, 0)
    val endPlants = breeder.breedForGenerations(gen0, PART_ONE_GENERATIONS, true)

    return endPlants.checksum()
}


fun day12partTwo(initialState: BooleanArray, rules: List<Pair<BooleanArray, Boolean>>): Long {
    val breeder = PlantBreeder(rules)
    val gen0 = PlantGeneration(initialState, 0)
    val endPlants = breeder.breedUntilStable(gen0)

    return endPlants.projectFutureChecksum(PART_TWO_GENERATIONS)
}


fun main(args: Array<String>) {
    val (initialState, rules) = inputPlants()

    println("${rules.size} rules")
    println("Initial state: ${PlantGeneration(initialState)}")

    println()
    println("Part One sum: ${day12partOne(initialState, rules)}")
    println()

    println("Part Two sum: ${day12partTwo(initialState, rules)}")
}
