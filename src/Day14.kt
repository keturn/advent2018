package net.keturn.advent2018

import kotlin.system.measureTimeMillis

val INITIAL_RECIPE_SCORES = listOf(3, 7)
const val N_COOKING_ELVES = 2
const val RECIPE_OUTPUT_SIZE = 10


fun day14Input(): String {
    return inputDirectory.resolve("14").readText().trim()
}


fun base10digits(n: Int): List<Int> {
    val reversedDigits = mutableListOf<Int>()
    var current = n
    do {
        reversedDigits.add(current % 10)
        current /= 10
    } while (current > 0)
    return reversedDigits.reversed()
}


class ChocolateScoreboard() {
    val scores = INITIAL_RECIPE_SCORES.toMutableList()
    val elfPositions = (0 until N_COOKING_ELVES).toMutableList()

    fun cookNext() {
        val currentSum = scores.slice(elfPositions).sum()

        val newDigits = base10digits(currentSum)

        scores.addAll(newDigits)

        for ((elfNumber, startingPoint) in elfPositions.withIndex()) {
            elfPositions[elfNumber] = (startingPoint + scores[startingPoint] + 1) % scores.size
        }
    }

    fun cookUntil(n: Int) {
        while (scores.size < n) {
            cookNext()
        }
    }

    fun resultsAfter(n: Int): String {
        cookUntil(n + RECIPE_OUTPUT_SIZE)
        return scores.slice((n) until (n + RECIPE_OUTPUT_SIZE)).joinToString("") { it.toString() }
    }

    fun findSequence(seq: List<Int>): Int {
        var current = 0
        do {
            cookUntil(current + seq.size)
            if (scores.subList(current, current + seq.size) == seq) {
                break
            }
            current++
        } while (true)
        return current
    }

    fun findSequence(seq: String): Int {
        return findSequence(seq.map { it.toString().toInt() })
    }
}


fun main(args: Array<String>) {
    val scoreboard = ChocolateScoreboard()
    val input = day14Input()
    println("INPUT: ${input}")

    println("Part One: ")
    println(scoreboard.resultsAfter(input.toInt()))

    val partTwoTime = measureTimeMillis {
        println("Part Two:")
        println(scoreboard.findSequence(input))
    }
    println("part two in ${partTwoTime} ms")
}
