package net.keturn.advent2018

import kotlin.experimental.or
import kotlin.experimental.xor
import kotlin.system.measureTimeMillis


const val CAPS_BIT = ('a'.toInt() xor 'A'.toInt()).toByte()


fun inputPolymer(): ByteArray {
    // input has a trailing newline!
    return inputDirectory.resolve("5").readBytes().sliceArray(0 until 50_000)
}


fun collapsePolymer(polymer: ByteArray, erasing: Byte? = null): ByteArray {
    val head = mutableListOf<Byte>()
    val erasingC: Byte? = if (erasing != null) erasing or CAPS_BIT else null

    for (nextUnit in polymer) {
        when {
            erasingC == (nextUnit or CAPS_BIT) -> Unit
            head.isEmpty() -> head.add(nextUnit)
            isReactive(head.last(), nextUnit) -> head.removeAt(head.size - 1)
            else -> head.add(nextUnit)
        }
    }

    return head.toByteArray()
}


fun isReactive(left: Byte, right: Byte): Boolean {
    return (left xor right) == CAPS_BIT
}


fun tryEliminatingAnything(polymer: ByteArray): Int {
    val outputCounts = ('a'..'z').toList().map { i -> collapsePolymer(polymer, erasing=i.toByte()).size }

    return outputCounts.min()!!
}


fun main(args: Array<String>) {
    val input = inputPolymer()
    println("loaded input polymer of size ${input.size}")

    var output: ByteArray? = null
    val partOneTime = measureTimeMillis {
        output = collapsePolymer(input)
    }

    println("Output of length ${output?.size} in ${partOneTime} ms.")

    var shortest: Int? = null

    val partTwoTime = measureTimeMillis {
        shortest = tryEliminatingAnything(output!!)
    }

    println("Output of length ${shortest} in ${partTwoTime} ms.")
}
