package net.keturn.advent2018

import java.lang.RuntimeException
import kotlin.system.measureTimeMillis

enum class LogTypes {
    BEGIN_SHIFT,
    SLEEP,
    WAKE
}

data class LogEntry(val time: String, val rawMessage: String) {
    companion object {
        fun fromLine(s: String): LogEntry {
            // example: [1518-02-28 00:47] falls asleep
            return LogEntry(time= s.substring(1..16), rawMessage = s.substring(19))
        }
    }

    val logType: LogTypes
        get() {
            return when {
                rawMessage == "falls asleep" -> LogTypes.SLEEP
                rawMessage == "wakes up" -> LogTypes.WAKE
                rawMessage.endsWith("begins shift") -> LogTypes.BEGIN_SHIFT
                else -> { throw RuntimeException("Bad log message: ${rawMessage}")}
            }
        }

    private val guardPattern = Regex("""#(\d+)""")

    val guardID: GuardID
        get() {
            val match = guardPattern.find(rawMessage)!!
            return GuardID.fromString(match.groups[1]!!.value)
        }

    val minute: Int
        get() = time.substringAfter(':').toInt()
}


inline class GuardID(private val id: Int) {
    companion object {
        fun fromString(s: String): GuardID = GuardID(s.toInt())
    }

    fun encodeMinute(minute: Int): Int {
        return id * minute
    }
}


fun inputLog(): List<LogEntry> {
    val infile = inputDirectory.resolve("4")
    val lines = infile.useLines {
        it.map { line -> LogEntry.fromLine(line) }.toList()
    }
    return lines.sortedBy { it.time }
}


class SleepLog {

    private val sleepPeriods = mutableListOf<IntRange>()

    fun log(sleep: Int, wake: Int) {
        sleepPeriods.add(sleep until wake)
    }

    fun total(): Int {
        return sleepPeriods.sumBy { it.count() }
    }

    fun histogram(): IntArray {
        val histogram = IntArray(60)
        for (nap in sleepPeriods) {
            for (minute in nap) {
                histogram[minute] += 1
            }
        }
        return histogram
    }

    fun favoriteNapMinute(): Pair<Int, Int> {
        val hist = histogram()
        val mostOccurances = hist.max()!!
        val minute = hist.indexOf(mostOccurances)
        return Pair(minute, mostOccurances)
    }

    fun sleepiestMinute(): Int {
        return favoriteNapMinute().first
    }
}


fun watchGuards(log: List<LogEntry>): MutableMap<GuardID, SleepLog> {
    val guardSleeps = mutableMapOf<GuardID, SleepLog>()

    var guardOnShift: GuardID? = null
    var sleepStart: Int? = null

    for (entry in log) {
        when (entry.logType) {
            LogTypes.BEGIN_SHIFT -> guardOnShift = entry.guardID
            LogTypes.SLEEP -> sleepStart = entry.minute
            LogTypes.WAKE -> guardSleeps.getOrPut(guardOnShift!!, ::SleepLog).log(sleepStart!!, entry.minute)
        }
    }

    return guardSleeps
}


fun sleepyGuard(sleeps: Map<GuardID, SleepLog>): Pair<GuardID, SleepLog> {
    val (sleepiestGuard, sleepLog) = sleeps.maxBy { (_, v) -> v.total() }!!
    return sleepiestGuard to sleepLog
}


fun consistentGuard(sleeps: Map<GuardID, SleepLog>): Pair<GuardID, Pair<Int, Int>> {
    val (guard, sleepLog) =  sleeps.maxBy { (_, sleepLog) -> sleepLog.favoriteNapMinute().second }!!
    return guard to sleepLog.favoriteNapMinute()
}


fun main(args: Array<String>) {
    val log = inputLog()
    println("${log.size} log entries")

    val totalTime = measureTimeMillis {
        val sleeps = watchGuards(log)
        val (guardID, sleepLog) = sleepyGuard(sleeps)

        val sleepyMinute = sleepLog.sleepiestMinute()

        val (predictableGuard, favSleep) = consistentGuard(sleeps)

        println("Sleepiest guard ${guardID} ${sleepLog.total()}")
        println("Sleepiest minute: ${sleepyMinute}")

        println("Part one code ${guardID.encodeMinute(sleepyMinute)}")
        println("Guard ${predictableGuard} often asleep at ${favSleep.first}.")
        println("Part two code: ${predictableGuard.encodeMinute(favSleep.first)}")
    }

    println("${totalTime} ms (including outputs)")
}

