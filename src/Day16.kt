package net.keturn.advent2018


@Suppress("EnumEntryName", "SpellCheckingInspection")
enum class ProcessorInstructions(private val f: (IntArray, Int, Int, Int) -> Unit) {
    addr({ registers, a, b, c -> registers[c] = registers[a] + registers[b] }),
    addi({ registers, a, b, c -> registers[c] = registers[a] + b }),
    multr({ registers, a, b, c -> registers[c] = registers[a] * registers[b] }),
    multi({ registers, a, b, c -> registers[c] = registers[a] * b }),
    banr({ registers, a, b, c -> registers[c] = registers[a] and registers[b] }),
    bani({ registers, a, b, c -> registers[c] = registers[a] and b }),
    borr({ registers, a, b, c -> registers[c] = registers[a] or registers[b] }),
    bori({ registers, a, b, c -> registers[c] = registers[a] or b }),
    setr({ registers, a, _, c -> registers[c] = registers[a] }),
    seti({ registers, a, _, c -> registers[c] = a }),
    gtir({ registers, a, b, c -> registers[c] = if (a > registers[b]) 1 else 0 }),
    gtri({ registers, a, b, c -> registers[c] = if (registers[a] > b) 1 else 0 }),
    gtrr({ registers, a, b, c -> registers[c] = if (registers[a] > registers[b]) 1 else 0 }),
    eqir({ registers, a, b, c -> registers[c] = if (a == registers[b]) 1 else 0 }),
    eqri({ registers, a, b, c -> registers[c] = if (registers[a] == b) 1 else 0 }),
    eqrr({ registers, a, b, c -> registers[c] = if (registers[a] == registers[b]) 1 else 0 });

    operator fun invoke(registers: IntArray, a: Int, b: Int, c: Int) = f(registers, a, b, c)
}


data class ProcessorSample(val stateBefore: IntArray, val input: IntArray, val stateAfter: IntArray) {
    init {
        assert(stateBefore.size == 4)
        assert(input.size == 4)
        assert(stateAfter.size == 4)
    }

    fun testCompatibility(): Set<ProcessorInstructions> {
        val compatible = ProcessorInstructions.values().filter { opcode ->
            val state = stateBefore.copyOf()
            opcode(state, input[1], input[2], input[3])
            stateAfter.contentEquals(state)
        }
        return compatible.toSet()
    }

    val opcode: Int get() = input[0]
}


fun numbersInBracketsAfterColon(s: String): IntArray {
    val listyPartOfString = s.split(":")[1].trim(' ', '[', ']')
    return listyPartOfString.split(',').map { it.trim().toInt() }.toIntArray()
}


fun inputProcessorSamples(): Pair<List<ProcessorSample>, List<IntArray>> {
    return inputDirectory.resolve("16").useLines { lineSeq ->
        val lines = lineSeq.iterator()
        val samples = mutableListOf<ProcessorSample>()
        val instructions = mutableListOf<IntArray>()

        do {
            val line = lines.next()
            if (line.isBlank()) {
                continue
            }

            if (line.startsWith("Before:")) {
                val beforeValues = numbersInBracketsAfterColon(line)
                val instruction = lines.next().split(' ').map(String::toInt).toIntArray()
                val afterValues = numbersInBracketsAfterColon(lines.next())
                samples.add(ProcessorSample(beforeValues, instruction, afterValues))
            } else {
                // Must be Part 2
                break
            }
        } while (true)

        lines.forEachRemaining {
            instructions.add(it.split(' ').map(String::toInt).toIntArray())
        }

        Pair(samples, instructions)
    }
}


fun examineSamples(samples: List<ProcessorSample>): Int {
    val compatForEach = samples.map { it.testCompatibility().size }
    return compatForEach.count { it >= 3 }
}


class Processor(private val opcodeMap: Map<Int, ProcessorInstructions>) {
    val state = IntArray(4)

    fun run(instructions: Iterable<IntArray>) {
        instructions.forEach { (op, a, b, c) ->
            opcodeMap[op]!!(state, a, b, c)
        }
    }
}


fun assignOpcodes(samples: List<ProcessorSample>): Map<Int, ProcessorInstructions> {
    val opcodeMap = mutableMapOf<Int, ProcessorInstructions>()

    val compatForEach = samples.map { it to it.testCompatibility() }

    val remainingSamples = compatForEach.toMutableList()

    resort@ do {
        val knownInstructions = opcodeMap.values.toSet()
        remainingSamples.removeAll { knownInstructions.containsAll(it.second) }
        remainingSamples.sortBy { (_, compats) ->
            compats.count { it !in knownInstructions } }

        for ((sample, compatible) in remainingSamples) {
            val possibleInstructions = compatible.minus(knownInstructions)
            if (possibleInstructions.size == 1) {
                val instruction = possibleInstructions.single()
                opcodeMap[sample.opcode] = instruction
                println("Assigned ${sample.opcode} = $instruction")
            }
            continue@resort
        }
    } while (remainingSamples.isNotEmpty())

    return opcodeMap
}


fun main(args: Array<String>) {
    val (samples, partTwoInput) = inputProcessorSamples()

    println("Part One: ${samples.size} samples")

    val matchThreeCount = examineSamples(samples)

    println("Match three or more: $matchThreeCount")

    println("Part Two: ${partTwoInput.size} instructions")
    val opcodeMap = assignOpcodes(samples)

    val processor = Processor(opcodeMap)
    processor.run(partTwoInput)
    println(processor.state.toList())
}
