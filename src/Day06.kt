package net.keturn.advent2018

import kotlin.math.abs
import kotlin.system.measureTimeMillis

const val SAFE_DISTANCE = 10_000

data class Coordinate(val x: Int, val y: Int) {
    companion object {
        fun fromString(s: String): Coordinate {
            val (x, y) = s.split(',').map { it.trim().toInt() }
            return Coordinate(x, y)
        }
    }

    fun distance(other: Coordinate): Int {
        return abs(this.x - other.x) + abs(this.y - other.y)
    }

    fun closest(others: Collection<Coordinate>): Coordinate? {
        val distances = others.map { it to this.distance(it) }
        val (_, minDistance) = distances.minBy { (_, distance) -> distance}!!
        return distances.singleOrNull { (_, distance) -> distance == minDistance }?.first
    }
}


fun inputCoords(): List<Coordinate> {
    val infile = inputDirectory.resolve("6")
    return infile.useLines { lines ->
        lines.map(Coordinate.Companion::fromString).toList()
    }
}


fun findAreas(coordinates: List<Coordinate>): Map<Coordinate, Int> {
    val minX = coordinates.minBy { it.x }!!.x
    val maxX = coordinates.maxBy { it.x }!!.x
    val minY = coordinates.minBy { it.y }!!.y
    val maxY = coordinates.maxBy { it.y }!!.y

    val everyplace = (minX..maxX).flatMap { x -> (minY..maxY).map { y-> Coordinate(x, y)}}

    val closestMap = everyplace.map { place -> place.closest(coordinates) }

    return closestMap.filterNotNull().groupingBy { it }.eachCount()
//    val closestMap = everyplace.parallelStream().map { place -> place.closest(coordinates) }
//    val areaCounts = closestMap.filter(Objects::nonNull).collect(Collectors.groupingByConcurrent({it}, Collectors.counting())).toMap()
//    @Suppress("UNCHECKED_CAST")
//    return areaCounts as Map<Coordinate, Long>
}

fun findNonInfiniteAreas(coordinates: List<Coordinate>): Map<Coordinate, Int> {
    val allAreas = findAreas(coordinates)
    return allAreas.filterNot { (center) -> isInfinite(center, coordinates) }
}


fun isInfinite(areaAround: Coordinate, allSites: List<Coordinate>): Boolean {
    val openNorth = allSites.none { blocker ->
        (blocker.y < areaAround.y) && (abs(blocker.x - areaAround.x) <= (areaAround.y - blocker.y) )
    }
    val openSouth = allSites.none { blocker ->
        (blocker.y > areaAround.y) && (abs(blocker.x - areaAround.x) <= (blocker.y - areaAround.y) )
    }
    val openEast = allSites.none { blocker ->
        (blocker.x > areaAround.x) && (abs(blocker.y - areaAround.y) <= (blocker.x - areaAround.x) )
    }
    val openWest = allSites.none { blocker ->
        (blocker.x < areaAround.x) && (abs(blocker.y - areaAround.y) <= (areaAround.x - blocker.x) )
    }
    return openNorth || openSouth || openEast || openWest
}

fun findProximity(coordinates: List<Coordinate>): Int {
    val minX = coordinates.minBy { it.x }!!.x
    val maxX = coordinates.maxBy { it.x }!!.x
    val minY = coordinates.minBy { it.y }!!.y
    val maxY = coordinates.maxBy { it.y }!!.y

    val everyplace = (minX..maxX).flatMap { x -> (minY..maxY).map { y-> Coordinate(x, y)}}

    return everyplace.count { spot -> coordinates.sumBy { it.distance(spot) } < SAFE_DISTANCE }
}

fun main(args: Array<String>) {
    val inputs = inputCoords()

    println("${inputs.size} inputs")

    var biggestArea: Int? = null
    val areaTime = measureTimeMillis {
        val areas = findNonInfiniteAreas(inputs)
        biggestArea = areas.maxBy { it.value }?.value
    }
    println("Biggest non-infinte: ${biggestArea} [$areaTime ms]")

    var safeArea: Int? = null
    val safeTime = measureTimeMillis {
        safeArea = findProximity(inputs)
    }
    println("Safe area: ${safeArea} [$safeTime ms]")
}
