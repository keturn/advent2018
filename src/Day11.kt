package net.keturn.advent2018

import kotlin.system.measureTimeMillis

const val GRID_SIZE = 300

fun serialNumber(): Int {
    return inputDirectory.resolve("11").readText().toInt()
}


private fun hundredsDigit(n: Int): Int {
    return (n%1000) / 100
}


fun powerAtPoint(serial: Int, x: Int, y: Int): Int {
    val rackID = x + 10
    val bloo = (rackID * y  + serial) * rackID
    return hundredsDigit(bloo) - 5
}


fun searchGrid3(serial: Int): Coordinate {
    val everyplace = (1..(GRID_SIZE - 2)).flatMap { x -> (1..(GRID_SIZE - 2)).map { y-> Coordinate(x, y)}}

    return everyplace.maxBy { (x, y) ->
        powerAtPoint(serial, x, y) + powerAtPoint(serial, x + 1, y) + powerAtPoint(serial, x + 2, y) +
        powerAtPoint(serial, x, y + 1) + powerAtPoint(serial, x + 1, y + 1) + powerAtPoint(serial, x + 2, y + 1) +
        powerAtPoint(serial, x, y + 2) + powerAtPoint(serial, x + 1, y + 2) + powerAtPoint(serial, x + 2, y + 2)
    }!!
}


fun powerInGrid(serial: Int, x: Int, y: Int, size: Int): Int {
    val everyplace = (x until (x+size)).flatMap { ix -> (y until (y + size)).map { iy-> Coordinate(ix, iy)}}.parallelStream()

    return everyplace.mapToInt { (ix, iy) -> powerAtPoint(serial, ix, iy)}.sum()
}


fun searchGridAny(serial: Int, size: Int): Pair<Int, Coordinate> {
    val everyplace = (1..(GRID_SIZE - size + 1)).flatMap { x -> (1..(GRID_SIZE - size + 1)).map { y-> Coordinate(x, y)}}

    val powerAtEveryplace = everyplace.parallelStream().map { coord -> powerInGrid(serial, coord.x, coord.y, size) to coord}

    val mostPower = powerAtEveryplace.max { a, b -> a.first.compareTo(b.first) }.get()

    println("grid size $size $mostPower")
    return mostPower
}


private fun <E, V: Comparable<V>> List<E>.maxValueBy(f: (E) -> V): Pair<V, E>? {
    if (isEmpty()) {
        return null
    }
    var maxSoFar: Comparable<V> = f(this[0])
    var inputOfMaxSoFar = this[0]
    for (i in this.listIterator(1)) {
        val current = f(i)
        if (maxSoFar < current) {
            maxSoFar = current
            inputOfMaxSoFar = i
        }
    }
    @Suppress("UNCHECKED_CAST")
    return (maxSoFar to inputOfMaxSoFar) as Pair<V, E>
}


fun searchAllSizes(serial: Int): Pair<Int, Coordinate> {
    val allSizes = (1 until GRID_SIZE).toList()

    val powerForSize =  allSizes.map { size -> searchGridAny(serial, size) }
    val mostPowerGrid = powerForSize.maxBy { (power, _) -> power }!!
    val mostPowerSize = powerForSize.indexOf(mostPowerGrid) + 1
    return mostPowerSize to mostPowerGrid.second
}


fun main(args: Array<String>) {
    val serial = serialNumber()

    lateinit var mostPowerAt: Coordinate
    val searchTime = measureTimeMillis {
        mostPowerAt = searchGrid3(serial)
    }
    println("Most power at $mostPowerAt [$searchTime ms]")
    println(powerAtPoint(serial, mostPowerAt.x, mostPowerAt.y))

    println("Part Two:")
    println(searchAllSizes(serial))
}
