package net.keturn.advent2018

import kotlin.system.measureTimeMillis

private fun inputLines(): List<String> {
    val infile = inputDirectory.resolve("2")
    return infile.readLines()
}


fun dayTwoPartOne(codes: List<String>): Int {
    val codeBits: Collection<Pair<Boolean, Boolean>> = codes.map(::checkCode)
    val sums = codeBits.fold(Pair(0, 0)) { totals, bits ->
        Pair(
            totals.first + if (bits.first) 1 else 0,
            totals.second + if (bits.second) 1 else 0
        )
    }
    return sums.first * sums.second
}


fun checkCode(code: String): Pair<Boolean, Boolean> {
    val letterCounts = code.groupingBy { it }.eachCount()

    return Pair(letterCounts.containsValue(2), letterCounts.containsValue(3))
}


fun isDistanceOne(a: String, b: String): Boolean {
    var difference = false
    for (i in 0 until a.length) {
        if (a[i] != b[i]) {
            if (difference) {
                return false  // difference > 1
            } else {
                difference = true
            }
        }
    }
    return difference
}


fun dayTwoPartTwo(codes: List<String>): String {
    val pairIndicies = pairsOfTwo(codes.size)

    val nears = pairIndicies.filter { (i, j) -> isDistanceOne(codes[i], codes[j]) }

    val (codeOneIndex, codeTwoIndex) = nears.single()
    println("$codeOneIndex\t${codes[codeOneIndex]}")
    println("$codeTwoIndex\t${codes[codeTwoIndex]}")

    return commonLetters(codes[codeOneIndex], codes[codeTwoIndex])
}


fun pairsOfTwo(size: Int): Array<IntArray> {
    val outSize = size * (size - 1) / 2
    val pairs = Array(outSize) {IntArray(2)}

    var outIndex = 0
    for (i in 0 until size) {
        for (j in i + 1 until size) {
            pairs[outIndex][0] = i
            pairs[outIndex][1] = j
            outIndex += 1
        }
    }
    return pairs
}


fun commonLetters(s1: String, s2: String): String {
    return s1.filterIndexed { index, c -> s2[index] == c }
}


fun main(args: Array<String>) {
    val codes = inputLines()

    var checksum: Int? = null
    val partOneTime = measureTimeMillis {
        checksum = dayTwoPartOne(codes)
    }
    println("Part One: ${checksum} in ${partOneTime} ms")

    var code: String? = null
    for (i in 0..6) {
        val time = measureTimeMillis {
            code = dayTwoPartTwo(codes)
        }
        println("Part Two: ${code} in ${time} ms")
    }
}
