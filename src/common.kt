package net.keturn.advent2018

import java.io.File

internal val inputDirectory = File("inputs")