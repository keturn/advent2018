package net.keturn.advent2018

import java.util.*
import kotlin.system.measureTimeMillis

val MINIMUM_DURATION = TimeDelta(61)
const val FASTEST_STEP_NAME = 'A'
const val WORKER_COUNT = 5


inline class TimeDelta(internal val seconds: Int) {
    operator fun plus(other: TimeDelta): TimeDelta {
        return TimeDelta(this.seconds + other.seconds)
    }

}


inline class Time(val seconds: Int): Comparable<Time> {
    override fun compareTo(other: Time): Int {
        return this.seconds.compareTo(other.seconds)
    }

    operator fun plus(duration: TimeDelta): Time {
        return Time(this.seconds + duration.seconds)
    }
}


data class Dependency(val step: Char, val requirement: Char) {
    companion object {
        fun fromDescription(s: String): Dependency {
            return Dependency(s[36], s[5])
        }
    }
}


fun inputDeps(): List<Dependency> {
    val infile = inputDirectory.resolve("7")
    return infile.useLines { lines ->
        lines.map(Dependency.Companion::fromDescription).toList()
    }
}


class Step(val name: Char) {
    val requirements = mutableSetOf<Step>()

    infix fun requires(step: Step): Step {
        requirements.add(step)
        return step
    }

    override fun toString(): String {
        return "<Step ${name}>"
    }

    fun duration(): TimeDelta {
        return MINIMUM_DURATION + TimeDelta(this.name - FASTEST_STEP_NAME)
    }
}


private fun defineSteps(deps: Iterable<Dependency>): List<Step> {
    val allSteps = mutableMapOf<Char, Step>()

    fun byName(name: Char): Step = allSteps.getOrPut(name) { Step(name) }

    deps.forEach { (stepName, reqName) -> byName(stepName) requires byName(reqName) }
    val stepList = allSteps.values.sortedBy { it.name }
    return stepList
}


class StepTracker(val allSteps: List<Step>) {

    val finishedSteps = LinkedHashSet<Step>(26)

    companion object {
        fun fromDependencies(deps: Iterable<Dependency>): StepTracker {
            return StepTracker(defineSteps(deps))
        }
    }

    fun next(): Step? {
        return allSteps.firstOrNull { (it !in finishedSteps) and finishedSteps.containsAll(it.requirements) }
    }

    fun completeStep(step: Step) {
        finishedSteps.add(step)
    }
}


class StepTrackerWithDurations(val allSteps: List<Step>) {

    val finishedSteps = LinkedHashSet<Step>(26)
    val inProgressSteps = mutableMapOf<Step, Time>()
    var currentTime = Time(0)

    companion object {
        fun fromDependencies(deps: Iterable<Dependency>): StepTrackerWithDurations {
            return StepTrackerWithDurations(defineSteps(deps))
        }

    }

    fun next(): Step? {
        return allSteps.firstOrNull {
            (it !in finishedSteps) and
                    (it !in inProgressSteps) and
                    finishedSteps.containsAll(it.requirements)
        }
    }

    fun beginStep(step: Step) {
        inProgressSteps[step] = currentTime + step.duration()
    }

    fun nextTimer(): Time {
        return inProgressSteps.minBy { it.value }!!.value
    }

    fun advanceTimeTo(time: Time): Set<Step> {
        currentTime = time
        val finishedNow = this.completedStepsInProgress()
        finishedNow.forEach { inProgressSteps.remove(it) }
        finishedSteps.addAll(finishedNow)
        return finishedNow
    }

    fun completedStepsInProgress(): Set<Step> {
        return this.inProgressSteps.filterValues { it <= currentTime }.keys
    }

    fun isAllDone(): Boolean {
        return finishedSteps.size == allSteps.size
    }
}


fun build(stepTracker: StepTracker): String {
    do {
        val nextStep = stepTracker.next() ?: break
        println("Next: ${nextStep}")
        stepTracker.completeStep(nextStep)
    } while (true)
    return stepTracker.finishedSteps.map { it.name }.joinToString("")
}


fun buildWithElves(stepTracker: StepTrackerWithDurations) {
    var availableWorkers = WORKER_COUNT
    do {
        do {
            val nextStep = stepTracker.next() ?: break
            stepTracker.beginStep(nextStep)
            availableWorkers--
        } while (availableWorkers > 0)
        val finishedSteps = stepTracker.advanceTimeTo(stepTracker.nextTimer())
        availableWorkers += finishedSteps.size
    } while (!stepTracker.isAllDone())
}


fun main(args: Array<String>) {
    val dependencies = inputDeps()

    lateinit var resultOne: String
    val partOneTime = measureTimeMillis {
        val tracker = StepTracker.fromDependencies(dependencies)
        resultOne = build(tracker)
    }
    println("Part One: ${resultOne} ${partOneTime} ms")

    lateinit var trackerWithDurations: StepTrackerWithDurations
    val partTwoTime = measureTimeMillis {
        trackerWithDurations = StepTrackerWithDurations.fromDependencies(dependencies)
        buildWithElves(trackerWithDurations)
    }
    println("Part Two: ${trackerWithDurations.currentTime} ${partTwoTime} ms")
}
