package net.keturn.advent2018

import kotlin.system.measureTimeMillis

/* cycle() from Kotlin Slack. Thanks Roman Elizarov */
fun <T> Iterable<T>.cycle() = sequence { while(true) { yieldAll(this@cycle) } }


private fun inputLines(): List<String> {
    val infile = inputDirectory.resolve("1")
    return infile.readLines()
}

internal fun parseLine(line: String): Int {
    return line.toInt()
}


fun partOne(): Int {
    val lines = inputLines()
    val deltas = lines.map(::parseLine)
    val frequency = deltas.sum()
    return frequency
}

const val TOO_MANY = 9e12

fun partTwo(): Int {
    val lines = inputLines()
    val deltas = lines.map(::parseLine)

    val seenFrequencies = mutableSetOf<Int>(0)
    var currentFrequency = 0

    for ((index, delta) in deltas.cycle().withIndex()) {
        currentFrequency += delta
        if (currentFrequency in seenFrequencies) {
            println("# Part two done after ${index} rounds")
            return currentFrequency
        } else {
            seenFrequencies.add(currentFrequency)
        }
        if (index > TOO_MANY) {
            throw RuntimeException("Too many iterations ${index}")
        }
    }
    throw RuntimeException("unreachable!")
}


fun main(args: Array<String>) {
    val time = measureTimeMillis {
        val frequencyOne = partOne()
        val frequencyTwo = partTwo()
        println("Part One: ${frequencyOne}")
        println("Part Two: ${frequencyTwo}")
    }
    println("time: ${time} ms")
}
