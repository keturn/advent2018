package net.keturn.advent2018

import kotlin.system.measureTimeMillis

fun inputLicenseNumbers(): List<Byte> {
    val infile = inputDirectory.resolve("8")
    return infile.readText().trimEnd().splitToSequence(' ').map { it.toByte() }.toList()
}


data class Node(val children: List<Node>, val metadata: List<Byte>) {
    fun walk(): Sequence<Node> {
        return sequence {
            yield(this@Node)
            for (child in children) {
                yieldAll(child.walk())
            }
        }
    }

    fun value(): Int {
        return if (children.isEmpty()) {
            metadata.sum()
        } else {
            metadata.mapNotNull { children.getOrNull(it.toInt() - 1) }.sumBy { it.value() }
        }
    }
}


fun makeNode(inputs: Iterator<Byte>): Node {
    val childCount = inputs.next()
    val dataCount = inputs.next()
    val children = (1..childCount).map { makeNode(inputs) }
    val metadata = inputs.take(dataCount.toInt()).toList()
    return Node(children, metadata)
}


private fun <T> Iterator<T>.take(count: Int): Sequence<T> {
    val iterator = this
    return sequence {
        for (i in 1..count) {
            yield(iterator.next())
        }
    }
}


fun main(args: Array<String>) {
    val inputs = inputLicenseNumbers()
    println("${inputs.size} inputs, first \"${inputs.first()}\" last \"${inputs.last()}\"")

    lateinit var node: Node
    val creationTime = measureTimeMillis {
        val inputIterator = inputs.listIterator()
        node = makeNode(inputIterator)
        assert(!inputIterator.hasNext()) { "${inputs.size - 1 - inputIterator.nextIndex()} inputs remain not converted to nodes!" }
    }

    val computeTime = measureTimeMillis {
        val metadataSum = node.walk().sumBy { it.metadata.sum() }

        println("Metadata sum: ${metadataSum}")
        println("Root node value: ${node.value()}")
    }
    println("$creationTime ms to create node, $computeTime ms to compute")
}


