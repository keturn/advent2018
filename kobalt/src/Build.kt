import com.beust.kobalt.*
import com.beust.kobalt.plugin.packaging.*
import com.beust.kobalt.plugin.application.*
import com.beust.kobalt.plugin.kotlin.*

val p = project {
    name = "advent2018"
    group = "net.keturn"
    artifactId = name
    version = "0.1"

    kotlinCompiler {
        version = "1.3.10"
    }
    
    sourceDirectories {
        path("src")
    }
    
    sourceDirectoriesTest {
        path("test")
    }

    dependencies {
        compile("org.jetbrains.kotlin:kotlin-stdlib:1.3.10")
        compile("org.jetbrains.kotlin:kotlin-stdlib-common:1.3.10")
        compile("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.3.10")
    }

    dependenciesTest {
        compile("org.testng:testng:6.11")
        compile("ch.tutteli.atrium:atrium-cc-en_GB-robstoll:0.7.0")
        compile("org.jetbrains.kotlin:kotlin-reflect:1.3.10")
    }

    assemble {
        jar {
        }
    }

    application {
        mainClass = "net.keturn.advent2018.Day01Kt.Main"
    }
}
