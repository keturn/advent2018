import ch.tutteli.atrium.api.cc.en_GB.toBe
import ch.tutteli.atrium.verbs.assert
import net.keturn.advent2018.collapsePolymer
import org.testng.annotations.Test


class TestCollapsePolymer {

    @Test
    fun `the example in the problem description`() {
        val input = "dabAcCaCBAcCcaDA".toByteArray()
        assert(String(collapsePolymer(input))).toBe("dabCBAcaDA")
    }
}