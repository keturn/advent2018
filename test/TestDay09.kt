import ch.tutteli.atrium.api.cc.en_GB.toBe
import ch.tutteli.atrium.verbs.expect
import net.keturn.advent2018.MarbleGame
import net.keturn.advent2018.PointValue
import org.testng.annotations.DataProvider
import org.testng.annotations.Test



class MarbleGameTest {
    @DataProvider(name = "Marble Game")
    fun marbleInputs(): Array<Array<Int>> {
        return arrayOf(
            arrayOf(9, 25, 32),
            arrayOf(10, 1618, 8317),
            arrayOf(13, 7999, 146_373),
            arrayOf(17, 1104, 2764),
            arrayOf(21, 6111, 54_718),
            arrayOf(30, 5807, 37_305)
        )
    }


    @Test(dataProvider = "Marble Game")
    fun `check the examples`(playerCount: Int, marbleValue: Int, expectedScore: Int) {
        val game = MarbleGame(playerCount, PointValue(marbleValue))
        game.play(printEachStep = false)
        expect(game.highScore()).toBe(PointValue(expectedScore))
    }
}