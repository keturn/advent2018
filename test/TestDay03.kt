package net.keturn.advent2018

import ch.tutteli.atrium.api.cc.en_GB.contains
import ch.tutteli.atrium.api.cc.en_GB.containsStrictly
import ch.tutteli.atrium.api.cc.en_GB.inAnyOrder
import ch.tutteli.atrium.api.cc.en_GB.notToBeNull
import ch.tutteli.atrium.api.cc.en_GB.only
import ch.tutteli.atrium.api.cc.en_GB.property
import ch.tutteli.atrium.api.cc.en_GB.toBe
import ch.tutteli.atrium.api.cc.en_GB.values
import ch.tutteli.atrium.verbs.assert
import org.testng.annotations.Test

class ClaimTest {

    @Test
    fun `no collision for far-apart things`() {
        val claim1 = Claim(1, 1, 2, 3, 4)
        val claim2 = Claim(2, 10, 20, 5, 6)

        assert(claim1.collision(claim2)).toBe(null)
    }

    @Test
    fun `no collision for adjacent things`() {
        val claim1 = Claim(1, 1, 2, 3, 4)
        val claim2 = Claim(2, 4, 2, 5, 6)

        assert(claim1.collision(claim2)).toBe(null)
    }

    @Test
    fun `collision of width 1`() {
        val TOP = 2
        val claim1 = Claim(100, 1, TOP, 3, 4)
        val claim2 = Claim(101, 3, TOP, 5, 6)

        assert(claim1.collision(claim2)).notToBeNull {
            property(Rect::width).toBe(1)

            property(Rect::top).toBe(TOP)
            property(Rect::left).toBe(claim2.left)
            property(Rect::height).toBe(claim1.height)
        }
    }

    @Test
    fun `the ones that failed us`() {
        val claims = listOf(
            Claim(id=20, left=23, top=189, width=18, height=10),
            Claim(id=14, left=25, top=80, width=25, height=20),
            Claim(id=18, left=38, top=70, width=26, height=22),
            Claim(id=40, left=39, top=185, width=24, height=10),
            Claim(id=54, left=47, top=90, width=13, height=11)
        )

        // expect conflicts:
        // 20 vs 40
        // 14 vs 18 - squarish
        // 14 vs 54 - tall and narrow
        // 18 vs 54 - short and wide

        val collisions = findCollisions(claims)

        val union = unionRects(collisions)
        val area = sumArea(union)

        assert(area).toBe(200)
    }
}


class RectTest {

    @Test
    fun `union of wholly overlapping rectangles is one rectangle`() {
        val rect1 = Rect(1, 2, 30, 40)
        val rect2 = Rect(10, 20, 5, 6)

        assert(rect1.collided(rect2)).notToBeNull { containsStrictly(rect1) }
    }


    /*            1
    *   0123456789012
    * 0 .............
    * 1 .............
    * 2 .XXXXXX......
    * 3 .XXXX**+++...
    * 4 .XXXX**+++...
    * 5 .XXXX**+++...
    * 6 .XXXXXX......
    * 7 .XXXXXX......
    * 8 .XXXXXX......
    * 9 .XXXXXX......
    *
    * */
    @Test
    fun `side overlapping rectangles return three`() {
        val input1 = Rect(1, 2, 6, 8)
        val input2 = Rect(5, 3, 5, 3)

        val left = Rect(input1.left, input1.top, 4, input1.height)
        val collision = Rect(5, input1.top, 2, input1.height)
        val right = Rect(7, input2.top, 3, 3)

        assert(input1.collided(input2)).notToBeNull {
            containsStrictly(left, collision, right)
        }
    }

    /*            1
    *   0123456789012
    * 0 .............
    * 1 .............
    * 2 .XXXXXXXX....
    * 3 .XXXXXXXX....
    * 4 .XXXX***X....
    * 5 .....+++.....
    * 6 .....+++.....
    * 7 .....+++.....
    * 8 .............
    *
    * */
    @Test
    fun `vertical partial overlap returns three`() {
        val input1 = Rect(1, 2, 8, 3)
        val input2 = Rect(5, 4, 3, 4)

        val left = Rect(input1.left, input1.top, 4, input1.height)
        val collision = Rect(5, input1.top, input2.width, 6)
        val right = Rect(8, input1.top, 1, input1.height)

        assert(input1.collided(input2)).notToBeNull {
            containsStrictly(left, collision, right)
        }
    }


    /*            1
    *   0123456789012
    * 0 .............
    * 1 .............
    * 2 .XXXXXX......
    * 3 .XXXXXX......
    * 4 .XXXXXX......
    * 5 .******......
    * 6 .******......
    * 7 .++++++......
    * 8 .++++++......
    * 9 .............
    *
    * */
    @Test
    fun `vertical stack returns one`() {
        val input1 = Rect(1, 2, 6, 5)
        val input2 = Rect(input1.left, 5, input1.width, 4)

        val collision = Rect(input1.left, input1.top, input1.width, 7)

        assert(input1.collided(input2)).notToBeNull {
            containsStrictly(collision)
        }
    }

    @Test
    fun `non-collision returns null`() {
        val rect1 = Rect(1, 2, 3, 4)
        val rect2 = Rect(10, 20, 5, 6)

        assert(rect1.collided(rect2)).toBe(null)
    }

    /*            1
    *   0123456789012
    * 0 .............
    * 1 .............
    * 2 .XXXXXX......
    * 3 .XXXX**+++...
    * 4 .XXXX**+++...
    * 5 .....+++++...
    * 6 .....+++++...
    * 7 .............
    *
    * */
    @Test
    fun `corner overlapping rectangles return three`() {
        val input1 = Rect(1, 2, 6, 3)
        val input2 = Rect(5, 3, 5, 4)

        val left = Rect(1,2,4, 3)
        val collision = Rect(5, 2, 2, 5)
        val right = Rect(7, 3, 3, 4)

        assert(input1.collided(input2)).notToBeNull {
            containsStrictly(left, collision, right)
        }
    }
}


class UnionRectTest {
    /*            1
    *   0123456789012
    * 0 .............
    * 1 .............
    * 2 .XXXXXX......
    * 3 .XXXX**+++...
    * 4 .XXXX**+++...
    * 5 .....+++++...
    * 6 .....++++%^..
    * 7 .........^^..
    * 8 .........^^..
    * 9 .............
    *
    * */
    @Test
    fun `multiple collisions are resolved`() {
        val input1 = Rect(1, 2, 6, 3)
        val input2 = Rect(5, 3, 5, 4)
        val input3 = Rect(9, 6, 2, 3)

        val area = 39

        assert(sumArea(unionRects(listOf(input1, input2, input3)))).toBe(area)
    }

    @Test
    fun `one thing is the only thing`() {
        val input1 = Rect(1, 2, 6, 3)

        assert(unionRects(listOf(input1))).containsStrictly(input1)
    }

    @Test
    fun `two non-colliding things`() {
        val input1 = Rect(1, 2, 6, 3)
        val input2 = Rect(9, 6, 2, 3)

        assert(unionRects(listOf(input1, input2))).containsStrictly(input1, input2)
    }

    @Test
    fun `two colliding things`() {
        val input1 = Rect(1, 2, 6, 3)
        val input2 = Rect(5, 3, 5, 4)

        assert(sumArea(unionRects(listOf(input1, input2)))).toBe(
            input1.area + input2.area - 2 * 2)
    }

    @Test
    fun `two disjoint groups`() {
        val inputs = listOf(
            Rect(left=39, top=189, width=2, height=6),
            Rect(left=38, top=80, width=12, height=12),  // 38..49 x 80..91
            Rect(left=47, top=90, width=13, height=2)    // 47..59 x 90..91
        )

        val flattened = unionRects(inputs)

        assert(flattened).contains.inAnyOrder.only.values(
            Rect(left=38, top=80, width=9, height=12),
            Rect(left=39, top=189, width=2, height=6),
            Rect(left=47, top=80, width=3, height=12),
            Rect(left=50, top=90, width=10, height=2)
        )

        assert(sumArea(flattened)).toBe((2*6) + (12*12) + (13*2) - (2*3))
    }

    @Test
    fun `two disjoint groups, reflected`() {
        val inputs = listOf(
            Rect(top=39, left=189, height=2,  width=6),
            Rect(top=38, left=80,  height=12, width=12),
            Rect(top=47, left=90,  height=3,  width=10),
            Rect(top=47, left=90,  height=13, width=2)
        )

        assert(sumArea(unionRects(inputs))).toBe(200)
    }


    /*
     *   0123456789012
     * 0 .............
     * 1 ...++........
     * 2 .XX**XXXXXXX.
     * 3 .XXXXXXXXXXX.
     * 4 .XXXXXX%%%XX.
     * 5 .......^^^...
     * 6 .............
     */
    @Test
    fun `multiple collisions against single rect`() {
        val input1 = Rect(1, 2, 11, 3)
        val input2 = Rect(3, 1, 2, 2)
        val input3 = Rect(7, 4, 3, 2)

        assert(sumArea(unionRects(listOf(input1, input2, input3)))).toBe(38)
    }
}
