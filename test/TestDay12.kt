package net.keturn.advent2018

import ch.tutteli.atrium.api.cc.en_GB.toBe
import ch.tutteli.atrium.verbs.assert
import org.testng.annotations.Test

private val exampleInitalState = "#..#.#..##......###...###"

private val exampleRules = listOf(
    poundToBoolArray("...##") to true,
    poundToBoolArray("..#..") to true,
    poundToBoolArray(".#...") to true,
    poundToBoolArray(".#.#.") to true,
    poundToBoolArray(".#.##") to true,
    poundToBoolArray(".##..") to true,
    poundToBoolArray(".####") to true,
    poundToBoolArray("#.#.#") to true,
    poundToBoolArray("#.###") to true,
    poundToBoolArray("##.#.") to true,
    poundToBoolArray("##.##") to true,
    poundToBoolArray("###..") to true,
    poundToBoolArray("###.#") to true,
    poundToBoolArray("####.") to true
)

class TestDay12 {
    @Test
    fun `check example`() {
        val initialState = poundToBoolArray(exampleInitalState)
        val rules = exampleRules

        assert(day12partOne(initialState, rules)).toBe(325)
    }

    @Test
    fun `isPlantAt with leftIndex = 0`() {
        val plants = PlantGeneration(exampleInitalState)
        assert(plants.isPlantAt(0)).toBe(true)
        assert(plants.isPlantAt(1)).toBe(false)
        assert(plants.isPlantAt(2)).toBe(false)
        assert(plants.isPlantAt(3)).toBe(true)
        assert(plants.isPlantAt(-1)).toBe(false)
    }


    @Test
    fun `isPlantAt with leftIndex = -2`() {
        val plants = PlantGeneration(exampleInitalState, -2)
        assert(plants.isPlantAt(-2)).toBe(true)
        assert(plants.isPlantAt(-1)).toBe(false)
        assert(plants.isPlantAt(0)).toBe(false)
        assert(plants.isPlantAt(1)).toBe(true)
        assert(plants.isPlantAt(-3)).toBe(false)
    }
}