package net.keturn.advent2018

import ch.tutteli.atrium.api.cc.en_GB.toBe
import ch.tutteli.atrium.verbs.assert
import org.testng.annotations.Test


class TestDay14 {
    @Test
    fun `finding recipes following index`() {
        val scoreboard = ChocolateScoreboard()

        assert(scoreboard.resultsAfter(5)).toBe("0124515891")
        assert(scoreboard.resultsAfter(9)).toBe("5158916779")
        assert(scoreboard.resultsAfter(18)).toBe("9251071085")
        assert(scoreboard.resultsAfter(2018)).toBe("5941429882")
    }

    @Test
    fun `finding index of sequence`() {
        val scoreboard = ChocolateScoreboard()

        assert(scoreboard.findSequence("01245")).toBe(5)
        assert(scoreboard.findSequence("51589")).toBe(9)
        assert(scoreboard.findSequence("01245")).toBe(5)
        assert(scoreboard.findSequence("92510")).toBe(18)
        assert(scoreboard.findSequence("59414")).toBe(2018)
    }
}