import ch.tutteli.atrium.api.cc.en_GB.toBe
import ch.tutteli.atrium.verbs.assert
import net.keturn.advent2018.LogEntry
import org.testng.annotations.Test

class TestLogEntry {
    @Test
    fun `a log line has a time and message`() {
        val input = "[1518-02-28 00:47] falls asleep"
        val entry = LogEntry.fromLine(input)
        assert(entry.time).toBe("1518-02-28 00:47")
        assert(entry.rawMessage).toBe("falls asleep")
    }
}