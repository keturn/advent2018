import ch.tutteli.atrium.api.cc.en_GB.containsStrictly
import ch.tutteli.atrium.api.cc.en_GB.isEmpty
import ch.tutteli.atrium.verbs.assert
import net.keturn.advent2018.Node
import org.testng.annotations.Test

class TestNode {
    @Test
    fun `short walk with no children`() {
        val node = Node(emptyList(), listOf(1, 2, 5))
        assert(node.walk().toList()).containsStrictly(node)
    }

    @Test
    fun `walk single child`() {
        val childNode = Node(emptyList(), listOf(7))
        val rootNode = Node(listOf(childNode), listOf(1, 2, 5))
        assert(rootNode.walk().toList()).containsStrictly(rootNode, childNode)
    }
}